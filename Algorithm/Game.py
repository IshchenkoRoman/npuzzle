import numpy as np

ACTIONS = {1: np.array([0, 1]), 2: np.array([0, -1]),
            3: np.array([-1, 0]), 4: np.array([1, 0])
        }

# 1- right, 2- left, 3- up, 4- down, q- quit
VALID_ACTIONS = ["1", "2", "3", "4", "q"]

class Game(object):

    def __init__(self, table=None):

        self.table = table
        self.table_shape = table.shape
        if self.table.size and len(self.table_shape) == 2 and self.table_shape[0] == self.table_shape[1]:
            # Find zero position
            self.currentZeroPos = np.argwhere(self.table == 0)[0]
        else:
            raise ValueError("Shape of matrix must be [N,N], given: [{0}]".format(self.table.shape))

    ##################
    # Getters/ Setters
    ##################

    def getPosition(self):

        return (self.currentZeroPos)

    def setPosition(self, next_position):

        self.currentZeroPos = next_position

    def getTable(self):

        return (self.table)

    def getValidMoves(self):

        currPos = self.getPosition()

        if 0 < currPos[0] < self.table_shape[0] - 1 and 0 < currPos[1] < self.table_shape[1] - 1:

            return (np.arange(1, 5))
        elif currPos[0] == 0 and 0 < currPos[1] < self.table_shape[1] - 1:

            return (np.array([1, 2, 4]))

        elif currPos[0] == self.table_shape[0] - 1 and 0 < currPos[1] < self.table_shape[1] - 1:

            return (np.arange(1, 2, 3))

        elif currPos[0] == currPos[1] == 0:

            return (np.array([1, 4]))
        elif currPos[0] == currPos[1] == self.table_shape[0] - 1:

            return (np.array([2, 3]))

        elif currPos[0] == self.table_shape[0] - 1 and currPos[1] == 0:

            return (np.array([1, 3]))

        elif currPos[0] == 0 and currPos[1] == self.table_shape[0] - 1:

            return (np.array([2, 4]))
        else:

            return (None)

    def makeMove(self, act):

        current_pos = self.getPosition()

        next_position = ACTIONS[act] + current_pos

        print(ACTIONS[act], current_pos, next_position)

        indexes = np.array([np.greater(next_position, [2,2], out=np.empty((2,))), np.less(next_position, [0,0], out=np.empty(2,))])
        if (next_position > -1).all()and not True in indexes:
            print("Move from {0} to position {1}".format(current_pos, next_position))

            table = self.getTable()
            table[tuple(current_pos)], table[tuple(next_position)] = \
                table[tuple(next_position)], table[tuple(current_pos)]
            self.setPosition(next_position)
            return (1)
        else:
            print("Invalid action {0} from {1} to position {2}".format(act, current_pos, next_position))
            return (0)

    def launchGame(self):

        inp = input("Enter move: [1- right, 2- left, 3- up, 4- down, q- quit]\n")

        while (inp != "q"):

            if inp in VALID_ACTIONS:
                self.makeMove(int(inp))
            else:
                print("Action doesn't exist!\n")
            print(self.getTable())

            inp = input("Enter move: [1- right, 2- left, 3- up, 4- down, q- quit]\n")
