from copy import deepcopy

from queue import PriorityQueue

import numpy as np

from . import MazeBuilder

import pdb

ACTIONS = {1: np.array([0, 1]), 2: np.array([0, -1]),
            3: np.array([-1, 0]), 4: np.array([1, 0])
        }

# 1- right, 2- left, 3- up, 4- down, q- quit
VALID_ACTIONS = ["1", "2", "3", "4", "q"]

class Graph(object):
    """
    Build graph of states game N-Puzzle and solve it

    Parameters:
        state (np.ndarray)- state of game board of N-Puzzle
        cost (int)- cost of node
        parent (object.Graph)- subsidiaries nodes of Graph

    Fields:
        self._state (np.ndarray)- current state of game board
        self._str_state (str)- string representations of self._state
        self._size (int)- size of board same as N
        self._state_shape (tuple of int)- full shape of board
        self._parent (object.Graph)- parent node of current node (for root it's None)
        self._childs (object.Graph)- child nodes of current node (for childs current node is parent)
        self._cost (int)- cost of curent node
        self._open_set (dict)- dictionary with key- hash as string representation of board,
                                value- (object.Graph) node, that cotain data of that state.
                                Needed for A* algorith (see n-puzzle.pdf)
        self._closed_set (dict)- see self._open_set documentation

        self._success (int)- int flag, that check if solution was founded
    """
    def __init__(self, state, cost, parent=None):

        self._state = state

        self._str_state = str(state)
        self._size = state.shape[0]
        self._state_shape = self._state.shape

        self._parent = parent
        self._childs = []
        self._cost = cost
        self._gCost = cost
        self._hCost = 0

        self._open_set = {}
        self._closed_set = {}

        self._success = 0

    ##################
    # Getters/ Setters
    ##################
    def getParent(self):

        return (self._parent)

    def setParent(self, parent):

        self._parent = parent

    def getChilds(self):

        return (self._childs)

    def getYoungestChild(self):

        """
        Get children with lowest cost of node.
        Parameters:
            None
        Return (None/ object.Graph):

            None:
                If node doesn't have childrens
            object.Graph:
                Otherwise children node of current state
        """

        count_childrens = len(self._childs)
        youngest_children = None

        if count_childrens:

            youngest_children = self._childs[0]
            smallest_value = youngest_children.getCostOfNode()
            for i in range(1, count_childrens):

                cost_current_child = self._childs[i].getCostOfNode()
                if cost_current_child < smallest_value:

                    youngest_children = self._childs[i]
                    smallest_value = cost_current_child
        else:
            print("EMPTY!")

        return (youngest_children)

    def getSelfNode(self):

        return (self)

    def getCostOfNode(self):

        return (self._cost)

    def setCostOfNode(self, final):

        cost = self._manhattanDistance(final)
        self._cost = np.sum(cost)

    def setCostOfNodeByValue(self, cost):

        self._cost = cost

    def getState(self):

        return (self._state)

    def getGCost(self):
        return (self._gCost)

    def getHCost(self):
        return (self._hCost)

    def setGCost(self, gCost):
        self._gCost = gCost

    def setHCost(self, HCost):
        self._hCost = HCost

    def setState(self, state, parent, final):

        """
        Init fields of current node.
        Parameters:
            state (np.ndarray):
                current state of game board
            parent (None/ object.Graph):
                parent of current node (if exist) else None
            final (np.ndarray):
                final state of game (solved N-Puzzle), used to get cost of current node
        Return (None/ object.Graph):

            None:
                If node doesn't have childrens
            object.Graph:
                Otherwise children node of current state
        """

        self._state = state
        self._str_state = str(state)
        self._size = self._state.shape[0]
        self._state_shape = self._state.shape

        self._parent = parent
        self.setCostOfNode(final)

    def _initOpenSet(self, final):

        """
        Init base condition of game, when valid readed board passed to constructor of Graph.
        First it get all valid moves from 0-position, then make clone of current sate with count
        same as valid moves, after that on copy of that states makes matched moves and
        init nodes with relevant state of maked move.
        Finaly all new states added to open set (self._open_set)
        Parameters:
            No input params
        Return:
            No returned data
        """
        selfNode = self.getSelfNode()

        self.appendOpenSet([selfNode])

    def getValidMoves(self, state):
        """
        In N-Puzzle we can move only white spaced cell (0), that swaped with neighbours.
        1- right, 2- left, 3- up, 4- down, q- quit

        Parameters:
            state (np.ndarray)
                State of board
        Return (np.ndarray/ None):
            np.ndarray:
                If valid move(s) exist
            None:
                Otherwise
        """

        currPos = np.argwhere(state == 0)[0]

        res = None

        if 0 < currPos[0] < self._state_shape[0] - 1 and 0 < currPos[1] < self._state_shape[1] - 1:
            res = np.arange(1, 5)
        elif currPos[0] == 0 and 0 < currPos[1] < self._state_shape[1] - 1:
            res = np.array([1, 2, 4])
        elif currPos[0] == self._state_shape[0] - 1 and 0 < currPos[1] < self._state_shape[1] - 1:
            res = np.array([1, 2, 3])
        elif currPos[1] == 0 and 0 < currPos[0] < self._state_shape[1] - 1:
            res = np.array([1, 3, 4])
        elif currPos[1] == self._state_shape[0] - 1 and 0 < currPos[0] < self._state_shape[1] - 1:
            res = np.array([2, 3, 4])
        elif currPos[0] == 0 and currPos[1] == 0:
            res = np.array([1, 4])
        elif currPos[0] == self._state_shape[0] - 1 and currPos[1] == self._state_shape[0] - 1:
            res = np.array([2, 3])
        elif currPos[0] == self._state_shape[0] - 1 and currPos[1] == 0:
            res = np.array([1, 3])
        elif currPos[0] == 0 and currPos[1] == self._state_shape[0] - 1:
            res = np.array([2, 4])
        else:
            print("Wooooooooops!")

        return (res)

    def getValidMovesState(self):
        """
        In N-Puzzle we can move only white spaced cell (0), that swaped with neighbours.
        1- right, 2- left, 3- up, 4- down, q- quit

        Parameters:
            state (np.ndarray)
                State of board
        Return (np.ndarray/ None):
            np.ndarray:
                If valid move(s) exist
            None:
                Otherwise
        """

        currPos = np.argwhere(self._state == 0)[0]

        res = None

        if 0 < currPos[0] < self._state_shape[0] - 1 and 0 < currPos[1] < self._state_shape[1] - 1:
            res = np.arange(1, 5)
        elif currPos[0] == 0 and 0 < currPos[1] < self._state_shape[1] - 1:
            res = np.array([1, 2, 4])
        elif currPos[0] == self._state_shape[0] - 1 and 0 < currPos[1] < self._state_shape[1] - 1:
            res = np.array([1, 2, 3])
        elif currPos[1] == 0 and 0 < currPos[0] < self._state_shape[1] - 1:
            res = np.array([1, 3, 4])
        elif currPos[1] == self._state_shape[0] - 1 and 0 < currPos[0] < self._state_shape[1] - 1:
            res = np.array([2, 3, 4])
        elif currPos[0] == 0 and currPos[1] == 0:
            res = np.array([1, 4])
        elif currPos[0] == self._state_shape[0] - 1 and currPos[1] == self._state_shape[0] - 1:
            res = np.array([2, 3])
        elif currPos[0] == self._state_shape[0] - 1 and currPos[1] == 0:
            res = np.array([1, 3])
        elif currPos[0] == 0 and currPos[1] == self._state_shape[0] - 1:
            res = np.array([2, 4])
        else:
            print("Wooooooooops!")

        return (res)

    def makeMoveState(self, act, state):
        """
        Make move with maped act. Swap empty cell with neighbour. Also check if action is valid,
        if it doesn't- state won't be changed.


        Parameters:
            state (np.ndarray)
                State of board
        Return (np.ndarray/ None):
            np.ndarray:
                If valid move(s) exist
            None:
                Otherwise
        """

        current_pos = np.argwhere(state == 0)[0]

        next_position = ACTIONS[act] + current_pos

        indexes = np.array([np.greater(next_position, [self._size, self._size], out=np.empty((2,))), np.less(next_position, [0,0], out=np.empty(2,))])
        if (next_position > -1).all() and not True in indexes:
            # print("Move from {0} to position {1}".format(current_pos, next_position))

            state[tuple(current_pos)], state[tuple(next_position)] = \
                state[tuple(next_position)], state[tuple(current_pos)]
            return (state)
        else:
            print("Invalid action {0} from {1} to position {2}".format(act, current_pos, next_position))
            return (None)

    ##################
    # Auxiliary funtions
    ##################

    def conceiveChildrens(self, childrens, cost):

        for node in childrens:
            self._childs.append(node)
            self._open_set[np.array_str(node._state)] = node

    def addToOpenSet(self, node):

        self._open_set[np.array_str(node._state)] = node

    def popOpenSet(self, element):

        return (self._open_set.pop(np.array_str(element._state)))

    def addToClosedSet(self, node):

        self._closed_set[np.array_str(node._state)] = node

    def popClosedSet(self, element):

        return (self._closed_set.pop(np.array_str(element._state)))

    def appendOpenSet(self, nodes):

        for node in nodes:

            self._open_set[np.array_str(node._state)] = node

    def getCheapsetFromOpen(self):

        nodes = self._open_set.values()
        poped_node = None
        value_poped_node = np.inf

        for node in nodes:
            if node.getCostOfNode() < value_poped_node:
                poped_node = node
                value_poped_node = poped_node.getCostOfNode()

        return (poped_node)

    ##################
    # Core logic
    ##################

    def _wrongPos(self, final, node):

        curr_state = node.getState()
        size = curr_state.shape[0] * curr_state.shape[1]
        return (size - np.count_nonzero(curr_state == final))

    def _linearConflictRowColumn(self, final, node):

        state = node.getState()
        shape = state.shape

        lc = 0
        for row in range(shape[0]):
            max = -1
            for column in range(shape[1]):
                piece = state[row][column]
                position = np.argwhere(piece == final)[0]
                if not piece:
                    continue
                # horisontal check
                if row == position[0]:
                    for c in range(column + 1, shape[1]):
                        if piece > state[row][c]:
                            lc += 1
                elif column == position[1]:
                    for r in range(row + 1, shape[0]):
                        if piece > state[r][column]:
                            lc += 1
        return (lc * 2)

    def _linearConflict(self, final, node):

        # taken from https://github.com/jDramaix/SlidingPuzzle/blob/master/src/be/dramaix/ai/slidingpuzzle/server/search/heuristic/LinearConflict.java

        heuristic = self._manhattanDistance(final, node)
        heuristic += self._linearConflictRowColumn(final, node)

        return (heuristic)

    def _euclideanDistance(self, final, node):

        delta = np.array([0 for i in range(self._size ** 2)])

        state = node.getState()
        for i in range(self._size):
            for j in range(self._size):

                if state[i][j]:
                    pos_ideal = np.argwhere(final == state[i][j])
                    diff = np.sum((np.array([i, j]) - pos_ideal) ** 2)

                    delta[i * self._size + j] = diff

        return (np.sum(delta))

    def _manhattanDistance(self, final, node):

        """
        Calculate Manhattan Distance. For more details see
        https://en.wikipedia.org/wiki/Taxicab_geometry
        """

        delta = np.array([0 for i in range(self._size ** 2)])
        state = node.getState()

        for i in range(self._size):
            for j in range(self._size):

                if state[i][j]:
                    pos_ideal = np.argwhere(final == state[i][j])
                    diff = np.sum(np.abs(np.array([i, j]) - pos_ideal))

                    delta[i * self._size + j] = diff

        return (np.sum(delta))

    def _getHeurisitic(self, type_heuristic):

        heuristic = self._manhattanDistance

        if type_heuristic == "Euclidean":
            heuristic = self._euclideanDistance
        elif type_heuristic == "WrongPos":
            heuristic = self._wrongPos
        elif type_heuristic == "LC":
            heuristic = self._linearConflict

        return (heuristic)

    def printPath(self, node):

        path = []

        while node:
            path.append(np.array(node._state))
            node = node.getParent()

        path = path[::-1]
        for i in path:

            print("{0}\n".format(i))

        path = np.asarray(path)
        return path

    def _expand(self, final, type_heuristic="Manhattan"):

        """
        Entry point. Launch path finder algorithm A* and print finded path.
        For details of algorith see pseudocode in n-puzzle.pdf
        https://en.wikipedia.org/wiki/Iterative_deepening_A*
        https://en.wikipedia.org/wiki/A*_search_algorithm
        Parameters:
            final (np.ndarray)
                Final state of board
        Return:
            No returned data
        """

        heuristic = self._getHeurisitic(type_heuristic)

        count_nodes = 1
        # http://theory.stanford.edu/~amitp/GameProgramming/ImplementationNotes.html
        while  self._open_set and not self._success:

            current = self.getCheapsetFromOpen()

            # is Final
            if np.array_equal(current.getState(), final):
                self._success = True
                print("YES!")
                path = self.printPath(current)
                return (path, count_nodes)
            else:
                current = self.popOpenSet(current)
                self.addToClosedSet(current)

            valid_pos = current.getValidMovesState()
            valid_pos_len = valid_pos.shape[0]

            cloneState = [self.makeMoveState(act, state) for act, state in zip(valid_pos, np.repeat(np.expand_dims(current._state, axis=0), valid_pos_len, axis=0))]
            cloneGraph = [deepcopy(Graph(i, current.getGCost(), None)) for i in cloneState]

            count_nodes += 4

            for neighbour in cloneGraph:
                state = neighbour.getState()

                cost = neighbour.getGCost() + 1
                if str(state) in self._open_set and cost < neighbour.getGCost():
                    self.popOpenSet(neighbour)
                if str(state) in self._closed_set and cost < neighbour.getGCost():
                    self.popClosedSet(neighbour)
                if str(state) not in self._open_set and str(state) not in self._closed_set:
                    neighbour.setGCost(cost)
                    # neighbour.setCostOfNodeByValue(neighbour.getGCost() + neighbour._manhattanDistance(final))
                    neighbour.setCostOfNodeByValue(neighbour.getGCost() + heuristic(final, neighbour))
                    self.addToOpenSet(neighbour)
                    neighbour.setParent(current)

    def _ida(self, final, type_heuristic="Manhattan"):

        heuristic = self._getHeurisitic(type_heuristic)
        currentNode = self.getSelfNode()
        nextCostBound = currentNode._manhattanDistance(final, currentNode)
        sol = None
        while (sol == None):

            count_nodes = 1

            currentCostBound = nextCostBound

            currentNode = deepcopy(currentNode)
            sol = self._depthFirstSearch(currentNode, currentCostBound, final, count_nodes, heuristic)

            nextCostBound += 1

        return (self.printPath(sol), count_nodes)

    def _depthFirstSearch(self, currentNode, currentCostBound, final, count_nodes, heuristic):

        if np.array_equal(currentNode.getState(), final):
            return (currentNode)

        valid_pos = currentNode.getValidMovesState()
        valid_pos_len = valid_pos.shape[0]

        cloneState = [self.makeMoveState(act, state) for act, state in zip(valid_pos, np.repeat(np.expand_dims(currentNode._state, axis=0), valid_pos_len, axis=0))]
        cloneGraph = [deepcopy(Graph(i, currentNode.getGCost() + 1, currentNode)) for i in cloneState]

        count_nodes += 4

        for node in cloneGraph:

            val = node.getGCost() + heuristic(final, node)

            if val <= currentCostBound:
                res = self._depthFirstSearch(node, currentCostBound, final, count_nodes, heuristic)
                if res != None:
                    return res

        return None
