import numpy as np

import pdb

class MazeBuilder(object):

	def __init__(self, size, type_maze="Golden ratio"):

		self._size = size
		self._blueprint = np.array([[0] * size for _ in range(size)])

		print(type_maze, type_maze == "Golden ratio")
		if type_maze == "Golden ratio":
			self.makeMaze()
		elif type_maze == "Classic":
			self.makeClassic()
		else:
			# That's bad
			raise ValueError("{0} doesn't support as type maze for  \
							MazeBuilder class in MazeBuilder.py. Use intead \
							[Classic | Golden ratio]".format(type_maze))

	def __str__(self):

		return (np.array2string(self._blueprint))

	def getSize(self):
		return (self._size)

	def getBlueprint(self):
		return (self._blueprint)

	def makeClassic(self):

		self._blueprint = np.arange(1, self._size ** 2)
		self._blueprint = np.append(self._blueprint, 0).reshape(self._size, self._size)

		return (self._blueprint)

	def makeMaze(self, debug=False):

		swap_sign = 1
		counter = 1
		amount = self._size ** 2
		value = 1

		i, j = 0, 0
		global_size = self._size
		local_size = self._size

		while True:
			if counter % 2:

				if swap_sign > 0:

					range_x = local_size + 1 if local_size != global_size else local_size
					for _ in range(local_size):
						self._blueprint[i][j] = value
						value += 1
						j += 1
					j -= 1
				else:

					range_x = 0 if j <= global_size - local_size - 1 else global_size - local_size - 2
					for _ in range(local_size):

						self._blueprint[i][j] = value
						value += 1
						j -= 1
					j += 1
			else:

				if swap_sign > 0:

					range_x = 0 if j <= self._size // 2 else global_size - local_size - 2
					for _ in range(local_size):
						self._blueprint[i][j] = value
						value += 1
						i += 1
					i -= 1
				else:

					range_x = 0 if i <= global_size - local_size - 2 else global_size - local_size - 2
					for _ in range(local_size):
						self._blueprint[i][j] = value
						value += 1
						i -= 1
					i += 1
			if value >= amount:
				break

			if local_size == 1:
				self._blueprint[i][j] = value
				value += 1

			if counter % 2:
				local_size -= 1
				i += swap_sign
			if not counter % 2:
				swap_sign *= -1
				j += swap_sign

			counter += 1
