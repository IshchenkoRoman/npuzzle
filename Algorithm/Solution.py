import numpy as np

from Algorithm.MazeBuilder import MazeBuilder
from .Graph import Graph
from Parser import RAP

from copy import deepcopy
import time

class Solution(object):

    def __init__(self, file_name, type_maze, type_heuristic, type_algo="A*", gui=None):

        self._size = 0
        self.file_name = file_name
        self._type = type_maze
        self._type_algo = type_algo
        self._type_heuristic = type_heuristic
        self._gui = gui
        self.currentState = np.array([])
        # That's bad[2]
        self._checkValidMaze(file_name)

        if self.currentState is not None:
            self.MazeBuilder = MazeBuilder(self._size, type_maze)
            if type_maze == "Classic":
                self.MazeBuilder.makeClassic()
            elif type_maze == "Golden ratio":
                self.MazeBuilder.makeMaze()

            self.Graph = None
            self.CurrentNode = None

            self.blueprint = self.MazeBuilder.getBlueprint()

            self._success = 0
            self._maze = self.blueprint[::-1]
        else:
            print("ERROR!")

    def _checkValidMaze(self, file_name):

        parser = RAP(file_name, self._type)

        if parser.processRAP(self._type):
            self.currentState = parser.getBoard()
            self._size = parser.getSize()
        else:
            self.currentState = None
            self._size = None
            if not self._gui:
                raise ValueError("Invalid map!")

    def guiInterface(self, array):

        self.blueprint = self.MazeBuilder.getBlueprint()

        self.Graph = Graph(array, 0)
        self.CurrentNode = self.Graph

        self.Graph._initOpenSet(self.blueprint)
        res = self.Graph._expand(self.blueprint)

        return (res)

    def launch(self, type_heuristic="Manhattan"):

        blueprint = self.blueprint

        self.Graph = Graph(self.currentState, 0)
        self.CurrentNode = self.Graph

        self.Graph._initOpenSet(blueprint)
        past_time = time.time()
        if self._type_algo == "A*":
            res, count_nodes = self.Graph._expand(blueprint, type_heuristic)
        else:
            res, count_nodes = self.Graph._ida(blueprint, type_heuristic)
        delta_time = time.time() - past_time
        return (res, count_nodes, delta_time, len(res))

    def launchIDA(self):

        blueprint = self.blueprint

        self.Graph = Graph(self.currentState, 0)
        self.CurrentNode = self.Graph

        self.Graph._initOpenSet(blueprint)
        res = self.Graph.ida(blueprint)

        return (res)
