import sys
import os

import numpy as np

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QDrag, QColor, QCursor, QIcon, QPainter, QPixmap

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

sys.path.append("..")
import Algorithm

from Algorithm.Solution import Solution
from .generated import *


class GUI(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.Solution = None
        self.init_data = None
        self.i = 0
        self.max_i = 0

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.nrows = 3
        self.ncols = 3

        # crap
        self.padding = 0 if self.nrows == 3 else 45 if self.nrows == 4 else 0
        self.padding = 0


        self.block_array = []
        self.SPACER = 0

        # self.ui.puzzleView.setFixedSize(self.ncols * 310 + 10, self.nrows * 310 + 10)
        self.ui.puzzleView.setVerticalScrollBarPolicy( Qt.ScrollBarAlwaysOff )
        self.ui.puzzleView.setHorizontalScrollBarPolicy( Qt.ScrollBarAlwaysOff )
        self.ui.puzzleView.horizontalScrollBar().disconnect()
        self.ui.puzzleView.verticalScrollBar().disconnect()

        self.gvRect = self.ui.puzzleView.rect()
        self.gvRect.setSize(QSize(500, 500))
        self.scene = QGraphicsScene(QRectF(self.gvRect), self.ui.puzzleView)
        self.ui.puzzleView.setScene(self.scene)

        self.ui.openButton.clicked.connect(self.openFile)
        self.ui.restartButton.clicked.connect(self.solvePuzzle)
        self.ui.prevButton.clicked.connect(self.prevState)
        self.ui.nextButton.clicked.connect(self.nextState)

        # Toggle button connect
        if self.ui.AlgorithmAButton.isChecked():
            self.ui.AlgorithmAButton.toggled.connect(self.restartWithAStart)
        if self.ui.AlgorithmIDAButton.isChecked():
            self.ui.AlgorithmIDAButton.toggled.connect(self.restartWithIDAStar)

        self.setWindowTitle("PyPuzzle")
        self.setWindowIcon(QIcon("icon.png"))

        # End

        self.create_blocks()
        self.show()

    def restartWithAStart(self):
        self.openFile()

    def restartWithIDAStar(self):
        self.openFile()

    def _checkRadioButton(self):

        type_maze = "Golden raio"
        type_algo = "A*"
        type_heuristic = "Manhattan"

        if self.ui.AlgorithmAButton.isChecked():
            type_algo = "A*"
        elif self.ui.AlgorithmIDAButton.isChecked():
            type_algo = "IDA"

        if self.ui.manhattanButton.isChecked():
            type_heuristic = "Manhattan"
        elif self.ui.euclideanButton.isChecked():
            type_heuristic = "Euqlidian"
        elif self.ui.linearConflictButton.isChecked():
            type_heuristic = "LC"
        elif self.ui.wrongPosButton.isChecked():
            type_heuristic = "WrongPos"

        if self.ui.goldenRatioButton.isChecked():
            type_maze = "Golden ratio"
        elif self.ui.classicMazeButton.isChecked():
            type_maze = "Classic"

        return (type_maze, type_algo, type_heuristic)

    def dropNumbers(self):

        for i in self.scene.items():
            if type(i) == type(QGraphicsSimpleTextItem()):
                self.scene.removeItem(i)

    def create_blocks(self):

        self.block_array = []
        for i in range(self.nrows):
            line_block = []
            for j in range(self.ncols):
                block = QGraphicsRectItem(0, 0, 150, 150)
                block.setPos(j * 150 + self.padding, i * 150 + self.padding)
                # block.setRect(j * 100, i * 100, 100, 100)

                self.scene.addItem(block)
                line_block.append(block)

            self.block_array.append(line_block)

    def prevState(self):
        if self.init_data is not None:
            self.i = self.i - 1 if self.i > 0 else 0
            state = self.init_data[self.i]
            self.dropNumbers()
            self.fillData(state)

    def nextState(self):
        if self.init_data is not None:
            self.i = self.i + 1 if self.i < self.max_i - 1 else self.max_i - 1
            state = self.init_data[self.i]
            self.dropNumbers()
            self.fillData(state)

    def showInfo(self, data, count_nodes, delta_time, len_graph):

        text = "Len of graph: {0}\nNodes explored: {1}\nTime spent: {2}s".format(len_graph, count_nodes, delta_time)

        self.ui.textInfo.setText(text)

    def fillData(self, data):

        color1 = Qt.lightGray
        color2 = Qt.gray
        last_color = color1
        color_table = {}

        keys = range(0, self.ncols ** 2)
        if self.ui.classicMazeButton.isChecked():
            for key in keys:
                color_table[key] = last_color
                last_color = color1 if last_color == color2 else color2
        else:
            for key in keys:
                if not key % 2:
                    color_table[key] = color2
                else:
                    color_table[key] = color1

        for j in range(self.ncols):
            for i in range(self.nrows):
                block = self.block_array[i][j]

                num = data[i][j]
                blockBoundRect = block.boundingRect()

                num = data[i][j]
                if num == self.SPACER:
                    text = str(num)
                    block.setBrush(QColor(Qt.green))
                elif self.ui.goldenRatioButton.isChecked():
                    text = str(num)
                    block.setBrush(QColor(color_table[j * self.nrows + i]))
                else:
                    text = str(num)
                    block.setBrush(QColor(color_table[num]))

                textItem = QGraphicsSimpleTextItem(text, block)
                font = textItem.font()
                font.setPixelSize(50)
                textItem.setFont(font)
                textItemBoundRect = textItem.boundingRect()
                new_x = blockBoundRect.width() / 2.0 - textItemBoundRect.width() / 2.0
                new_y = blockBoundRect.height() / 2.0 - textItemBoundRect.height() / 2.0
                textItem.setPos(new_x, new_y)

    def solvePuzzle(self, type_heuristic):
        if self.Solution and self.Solution.currentState is not None:
            self.init_data, count_nodes, delta_time, len_graph = self.Solution.launch(type_heuristic)
            self.i = 0
            self.max_i = len(self.init_data)
            print(self.init_data, self.init_data.shape)
            _, self.nrows, self.ncols = self.init_data.shape

            data = self.init_data[self.i]
            self.dropNumbers()
            self.showInfo(data, count_nodes, delta_time, len_graph)
            self.create_blocks()
            self.fillData(data)
        else:
            self.dropNumbers()
            self.showError()

    def showMatrix(self):
        if self.i < self.max_i:
            self.ui.label.setText("{0}\n".format(self.res[self.i]._state))
            self.i += 1

    def showError(self):

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setWindowTitle("Error!")
        msg.setText("Wooooooooops! Error!")
        msg.setDetailedText("Not valid file/ matrix. See logs in console")
        msg.exec_()

    def openFile(self, type_maze="Golden ratio", type_algo="A*", path=None):
        type_maze="Golden ratio"
        if not path:
            path, _ = QFileDialog.getOpenFileName(self, "Open [map].txt",
                                                    "", "Text files (*.txt)")
        if path:
            fname = QFile(path)
            print(path)
            if not fname.open(QFile.ReadOnly | QFile.Text):
                QMessageBox.warning(self, "Application", "cannot read file {0}:\n{1}".format(path, fname.errorString()))

                self.textFile = []
            else:
                type_maze, type_algo, type_heuristic = self._checkRadioButton()
                print("SOL ==========>", type_maze, type_algo, type_heuristic)
                self.Solution = Solution(file_name=path, type_maze=type_maze, type_heuristic=type_heuristic, type_algo=type_algo, gui="GUI")
                if self.Solution.currentState is not None:
                    self.dropNumbers()
                    self.solvePuzzle(type_heuristic)
                else:
                    self.showError()

if __name__ == "__main__":

    app = QApplication(sys.argv)
    ex = GUI()
    sys.exit(app.exec_())
