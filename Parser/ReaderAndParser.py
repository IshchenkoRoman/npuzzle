import os
import sys

import numpy as np

from .validator import checkClassic, checkGolden
from Algorithm.MazeBuilder import MazeBuilder

class RAP(object):

    """RAP - Reader And Parser

    Read bytes from current directory and sub directories. Print relevant errors
    if they exist. RAP have limits for size of file- 4950 bytes- it's about map of
    size 17x17  with white spaces. Final board stored in field self._board if all
    test passed as (np.ndarray), otherwise- it stored (None).

    Parameters:
        file_name (str):
            string path to file

    Fields:
        self._file_name (str)- string path to file
        self._board (np.ndarray/ None)- readed and parsed board
        self._current_dir (str)- path, where source was called
        self._path (str)- full path to file
    """

    def __init__(self, file_name, type):

        self._file_name = file_name

        self._type = type
        self._board = []
        self._size = 0
        self._current_dir = os.getcwd()

        self._path = file_name
        if sys.platform == "linux":
            if "/home/rishchen" not in file_name:
                self._path = "".join([self._current_dir, "/", self._file_name])
        elif sys.platform == "OS X":
            if "/Users/rishchen/" not in file_name: # or
                self._path = "".join([self._current_dir, "/", self._file_name])

    def getPath(self):
        return (self._path)

    def getSize(self):
        return (self._size)

    def getBoard(self):

        return (np.array(self._board))

    def getBlueprint(self):

        return (self._blueprint)

    def getType(self):

        return (self._type)

    def _checkIfExistPath(self, path):
        """
        Check if path exist
        Parameters:
            path (str):
                Path to file with file name

        Return (int):
            0:
                If path not exist
            1:
                If exist
        """
        if not os.path.exists(path):
            return (0)
        statinf = os.stat(path)

        return(1)

    def checkIfValuesValid(self, board):
        """
        Check if values valids: values must be half-interval of range [0 - size_board ** 2)
        Parameters:
            board (np.ndarray):
                Board with values of N-Puzzle with size N*N
        Return (int):
            0:
                If board doesn't satisfies the conditions of half-interval of range [0 - size_board ** 2)
            1:
                If does
        """
        copy_board = board[:]
        shape = copy_board.shape

        # if shape[0] not in range(3, 5) or shape[0] != shape[1]:
        #     return (0)

        if shape[0] != shape[1]:
            return (0)

        arange = np.arange(shape[0] ** 2)
        res = False if False in (arange == np.sort(copy_board.flatten())) else True
        return (res)

    def checkSharp(self, line):

        idx = line.find("#")
        line = line[:(idx if idx > 0 else len(line))]

        return (idx, line)

    def readFile(self, path):
        """
        Read file with path
        Parameters:
            path (str):
                string representation of path in current (sub)/directory
        Return (None/ np.ndarray): (ugly architecture)
            None:
                If symbol isn't digit
            np.ndarray:
                If all good
        """

        import pdb

        # pdb.set_trace()
        f = open(path, "r")
        for line in f:
            idx, line = self.checkSharp(line)
            if idx == -1 or idx > 4:
                line = line.split()
                if False in [x.isdigit() for x in line]:
                    f.close()
                    return (None)
                else:
                    line_board = [int(x) for x in line]
                    if len(line_board):
                        self._board.append(line_board)

        self._board = np.array(self._board, dtype=np.int16)

        return (self._board)

    def processRAP(self, type_maze):
        """
        Entry point for launching reading and parsing array. Call internal function
        for validate array and store in self._board as (nd.array). If something gone wrong-
        self._board store (None).
        Parameters:
            None input params
        Return (int):
            0:
                If errors occurs
            1:
                If checkers passed
        """

        path = self.getPath()
        status = 0

        status_exist = self._checkIfExistPath(path)
        if not status_exist:
            print("Error! Not valid file {0}".format(path.split("/")[-1]), file=sys.stderr)
            status = 0

        self._board = self.readFile(path)
        if type(self._board) == type(None):
            print("Error! File contain not digits!", file=sys.stderr)
            status = 0

        statust_numbers = self.checkIfValuesValid(self._board)
        if statust_numbers == 0:
            print("Error! File contain digits that out of range [0-{0}] Or shape not in range [3-5)!".format(self._board.shape[0] ** 2 - 1), file=sys.stderr)
            status = 0
            return (status)

        '''
        Here we need to check if solvable puzzle: for classic and main puzzle.
        '''

        if type_maze == "Classic":
            status= checkClassic(self._board)
        else:
            status = checkGolden(self._board)

        if not status:
            print("Wow! Not Valid puzzle or wrong puzzle mode has chosen!")
            self._board = None
        else:
            self._size = self._board.shape[0]
            print("Puzzle is valid!")
        return (status)

    def _inversion(self, board):

        inversion_c = 0
        board = self.getBoard().flatten()
        N = board.shape[0] ** 2 - 1
        D = board.shape[0] ** 2

        for curr in range(N):
            for i in range(curr + 1, D):
                if board[curr] > board[i] and (board[curr] != 0 and board[i] != 0):
                    inversion_c += 1
        return (inversion_c)

    def checkIsSolvableGolden(self, blueprint):

        board = self.getBoard()
        size = board.shape[1]
        inversion_puzzle = self._inversion(board)
        inversion_final = self._inversion(blueprint)

        if board.size % 2 == 0:
            inversion_puzzle += (size - int(np.where(blueprint == 0)[0] / size)) % 2
            inversion_final += (size - int(np.where(blueprint == 0)[0] / size)) % 2
        if (inversion_puzzle % 2) == (inversion_final % 2):
            return (True)
        return (False)

    def checkIsSolvableClassic(self, blueprint):
        """
        Taken from here: https://www.geeksforgeeks.org/check-instance-15-puzzle-solvable/
        Check if Classic N-puzzle is solvable.
        Parameters:
            None input params
        Return (int):
            0:
                If not sovable
            1:
                If it is
        """

        board = self.getBoard().flatten()
        N = board.shape[0]
        N_SQR = board.shape[0] ** 2

        def getInvCount(board):

            inv_count = 0

            for i in range(0, N_SQR - 1):
                for j in range(i + 1, N_SQR):
                    if board[j] and board[i] and board[i] > board[j]:
                        inv_count += 1
            return (inv_count)

        inversions_count = getInvCount(board.flatten())

        if (N & 1):
            return (not (N & inversions_count))

        positionX = N - list(zip(*np.where(board == 0)))[0]

        if (positionX & 1):
            return (not (inversions_count & 1))

        return (inversions_count & 1)

def main(args):

    rap = RAP(args.name[0])
    rap.processRAP()

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(prog="N-puzzle")
    parser.add_argument("-n", "--name", type=str, nargs=1, default="", help="Giff me name of f-f-f-file")
    parser.add_argument("--version", action="version", version="{0} 1.0".format(parser.prog))
    args = parser.parse_args()

    main(args)
