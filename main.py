import sys
import argparse

import numpy as np

from Algorithm.MazeBuilder import MazeBuilder
from Algorithm.Solution import Solution
from Algorithm.Game import Game

from Algorithm.Graph import Graph

from GUI.guiMain import GUI
from PyQt5.QtWidgets import QApplication

import time


def main(args):

	# https://courses.cs.washington.edu/courses/cse473/12sp/slides/
	# https://ac.els-cdn.com/S0004370201000923/1-s2.0-S0004370201000923-main.pdf?_tid=89fd38cd-3c96-4a4f-881b-9afd799b0855&acdnat=1546465814_ab6d847ec5fd3284d4ad672612ffeb98

	if args.gui == True:
	    app = QApplication(sys.argv)
	    ex = GUI()
	    sys.exit(app.exec_())
	else:
		try:
			sol = Solution(file_name=args.file, type_maze=args.maze, type_heuristic=args.heuristic, type_algo=args.algo)
			past = time.time()
			init_data, count_nodes, delta_time, len_graph = sol.launch(args.heuristic)
			print("Expanded nodes: {0}\nTime: {1}\nLen solution: {2}".format(count_nodes, delta_time, len_graph))
		except:
			print ("Error:", sys.exc_info()[1])


parser = argparse.ArgumentParser()

parser.add_argument("-f", "--file", help="file name, where writed matrix",
					type=str, default="None")
parser.add_argument("-g", "--gui", help="swith on gui program", default=False, action="store_true")
parser.add_argument("-he", "--heuristic", help="change heuristics from list:[Manhattan | Euclidean | Wrong Position (WronPos) | Linear Conflict (LC)]",
					default="Manhattan", type=str, metavar=["Manhattan | Euclidean |WrongPos | LC"], choices=["Manhattan", "Euclidean", "WrongPos", "LC"])
parser.add_argument("-m", "--maze", help="Get type of type maze from list:[Classic, Golden ratio]",
					default="Golden ratio", type=str, metavar=["Classic | Golden ratio"], choices=["Classic", "Golden ratio"])
parser.add_argument("-al", "--algo", help="Get type of type algorithm from list:[A*, IDA]",
					default="A*", type=str, metavar=["A* | IDA"], choices=["A*", "IDA"])
parser.add_argument("--version", action="version", version="{0} 1.0".format(parser.prog))
args = parser.parse_args()

main(args)
